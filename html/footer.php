<?php
if( !defined('PHURL' ) ) {
    header('HTTP/1.0 404 Not Found');
    exit();
}
ini_set('display_errors', 0);
?>
<h2>Browser Bookmarklets</h2>
<div id="bookmarklets">
<p>Drag these links to your browser toolbar.</p>
<p><a href="javascript:(function(){var%20ali=prompt('Enter%20a%20custom%20alias:');if(ali){location.href='<?php echo SITE_URL ?>/index.php?url='%20+%20escape(location.href)%20+%20'&alias='+ali;}})();" title="Shorten with a custom alias">Shorten with a custom alias</a><br />
<a href="javascript:void(location.href='<?php echo SITE_URL ?>/index.php?alias=&url='+escape(location.href))">Shorten without a custom alias</a></p>
</div>
</div>
<div id="footer">
<p>&copy; <?php echo date("Y"); ?> <?php echo SITE_TITLE ?> - Developed by <a href="http://www.topahead.com">TopAhead </a></p>
</div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-32385583-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</div>
</body>
</html>